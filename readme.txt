
Following a suggestion by Simon Willison's blog post "Dissecting
the Google Firefox Toolbar"[1], this module uses the same spell
checking service as the Firefox Google Toolbar[2].

Unlike Google's Web API spell checking service[3], the toolbar
service is _not_ limited to 10 words.

The following languages are supported by the spell check service,
however only English is currently enabled:

* Danish - da
* Dutch - nl
* English - en
* Finnish - fi
* French - fr
* German - de
* Italian - it
* Polish - pl
* Portuguese - pt
* Spanish - es
* Swedish - sv

[1] http://simon.incutio.com/archive/2005/07/08/toolbar
[2] http://toolbar.google.com/
[3] http://www.google.com/apis/reference.html#1_3
